FROM node:current-alpine
COPY . /home/node/chillax-bot/
WORKDIR /home/node/chillax-bot/
CMD node --trace-warnings /home/node/chillax-bot/index.js