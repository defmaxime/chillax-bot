module.exports = {
    name: 'game_poll',
    description: require('./string.json'),
    async execute(channel, language, roles_id) {
        const data = require('./'+this.name+'.json');
        const strings = require('./string.json')[language];
        const { MessageEmbed } = require('discord.js');

        const embedMessage = new MessageEmbed()
            .setColor(data.options.color)
            .setTitle(strings.title)
            .setDescription(strings.description)
        data.items.forEach(item => {
            embedMessage.description +=`${item.emoji}\t${item.text}\n`
        });
        
        channel.send({ embeds: [embedMessage] }).then(embedMessage => {
            data.items.forEach(item => {
                embedMessage.react(item.emoji);
            });
        })
    }
}