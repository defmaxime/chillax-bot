module.exports = {
    name: 'all_polls',
    description: require('./string.json'),
    async execute(channel, language) {
        const commands_order = require('./commands_order.json');
        await channel.send(strings.ping_text.before+"<@&"+roles_id.ppl_to_call_role+">"+strings.ping_text.after)

        Object.entries(commands_order).forEach(command => {
            console.log("execute "+command[0])
            channel.client.commands.get(command[0]).execute(channel, language, channel.client.roles_id);
        })
    }
}