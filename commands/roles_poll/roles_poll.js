module.exports = {
    name: 'roles_poll',
    description: require('./string.json'),
    async execute(channel, language, _, client, request_at_boot) {
        const data = require('./' + this.name + '.json');
        const strings = require('./string.json')[language];
        const { MessageEmbed } = require('discord.js');
        const current_channel = client.channels.cache.find(channel => channel.name === data.options.channel_name)

        const embedMessage = new MessageEmbed()
            .setColor(data.options.color)
            .setTitle(strings.title)
            .setDescription(strings.description)
        data.items.forEach(item => {
            embedMessage.description += `${item.emoji}\t${item.text}\n`
        });

        let message;
        if (!request_at_boot) {
            try {
                message = await current_channel.messages.fetch(data.last_poll_message_id);
                message.delete();
            } catch (error) {
                console.log("no previous message to remove")
            }
        } else {
            try {
                message = await current_channel.messages.fetch(data.last_poll_message_id);
                client.on('messageReactionAdd', async (reaction, user) => {
                    const reactionValid=await this.isReactionValid(reaction, user);
                    if(!reactionValid) return;
                    console.log("    reaction received from a human");
                    const last_poll_message_id = require('./' + this.name + '.json').last_poll_message_id;
                    if(reaction.message.id === last_poll_message_id){
                        console.log("    catched reaction on latest poll message !")
                        await this.addRoleToUser(client, reaction, user)
                    }
                });

                client.on('messageReactionRemove', async (reaction, user) => { 
                    if(!this.isReactionValid(reaction, user)) return;
                    console.log("    reaction removed by a human");
                    const last_poll_message_id = require('./' + this.name + '.json').last_poll_message_id;
                    if(reaction.message.id === last_poll_message_id){
                        console.log("    catched reaction on latest poll message !")
                        await this.removeRoleFromUser(client, reaction, user)
                    }
                });
                return
            } catch (error) {
                console.log("no previous roles poll to check")
                return
            }
        }

        current_channel.send({ embeds: [embedMessage] }).then(embedMessage => {
            data.items.forEach(item => {
                embedMessage.react(item.emoji);
            });
            data.last_poll_message_id = embedMessage.id
            require('fs').writeFileSync('commands/' + this.name + '/'+ this.name +'.json', JSON.stringify(data), 'utf8')

            client.on('messageReactionAdd', async (reaction, user) => {
                const reactionValid=await this.isReactionValid(reaction, user);
                if(!reactionValid) return;
                console.log("    reaction received from a human");
                const last_poll_message_id = require('./' + this.name + '.json').last_poll_message_id;
                if(reaction.message.id === last_poll_message_id){
                    console.log("    catched reaction on latest poll message !")
                    await this.addRoleToUser(client, reaction, user)
                }
            });

            client.on('messageReactionRemove', async (reaction, user) => { 
                if(!this.isReactionValid(reaction, user)) return;
                console.log("    reaction removed by a human");
                const last_poll_message_id = require('./' + this.name + '.json').last_poll_message_id;
                if(reaction.message.id === last_poll_message_id){
                    console.log("    catched reaction on latest poll message !")
                    await this.removeRoleFromUser(client, reaction, user)
                }
            });
        })
    },
    async isReactionValid(reaction, user){
        if (reaction.partial) await reaction.fetch();
        if (user.bot) return false;
        return true;
    },
    async addRoleToUser(client,reaction, user){
        const data = await this.getData(client, reaction, user);
        data.user_roles.add(data.role)
        console.log("role \""+data.role.name+"\" added to \""+user.username+"\"")
    },
    async removeRoleFromUser(client,reaction, user){
        const data = await this.getData(client, reaction, user);
        data.user_roles.remove(data.role)
        console.log("role \""+data.role.name+"\" removed from \""+user.username+"\"")
    },
    async findGuild(client,reaction){
        return await client.guilds.fetch(reaction.message.guildId)
    },
    async findRoleFromDB(reaction){
        const found_role = require('./' + this.name + '.json').items.filter(current_role =>
            current_role.emoji == reaction._emoji.name
        )
        if(found_role.length != 1){
            console.error("multiple roles found !")
            return
        }
        return found_role[0]
    },
    async getData(client,reaction,user){
        const current_guild = await client.guilds.fetch(reaction.message.guildId);
        const found_role    = await this.findRoleFromDB(reaction);
        const target_user   = await current_guild.members.cache.get(user.id);
        const target_role   = await current_guild.roles.cache.find(role => role.name === found_role.text)
        return {guild:current_guild,role:target_role,user_roles:target_user.roles}
    }
}