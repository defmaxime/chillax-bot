// Require the necessary discord.js classes
const { Client, Intents, Collection } = require('discord.js');
const { REST } = require('@discordjs/rest');
const { Routes } = require('discord-api-types/v9');
const { token } = require('./json_files/credentials.json');
const config = require('./json_files/config.json');
const localized = require('./json_files/verbose/'+config.language+'.json');
const fs = require('fs');

// Create a new client instance
const client = new Client({ intents: [Intents.FLAGS.GUILDS,Intents.FLAGS.GUILD_MESSAGES, Intents.FLAGS.GUILD_MESSAGE_REACTIONS] });

function getCommands(commandsFolder) {
  commands_set = new Collection();
  fs.readdirSync(commandsFolder).forEach(currentCommandFolder => {
    const js_files = fs.readdirSync(commandsFolder+currentCommandFolder).filter(file => file.endsWith('.js'))
    if(js_files.length > 1){
      console.error("The folder \""+commandsFolder+currentCommandFolder+"\" contains more than only one *.js file\nThe first one is used")
    }
    const command = require(commandsFolder+currentCommandFolder+"/"+js_files[0]);
    commands_set.set(command.name, command);
  });
  return commands_set;
}

function detectCommands(message) {
  return client.commands.filter(current_command => 
    message.content.includes(localized.commands_shortcuts[current_command.name]) 
  )
}

// When the client is ready, run this code (only once)
client.once('ready', () => {
	console.log('Ready!');
  client.home_channel = client.channels.cache.find(channel => channel.name === config.home_channel.name)
  client.home_channel.send(localized.waking_up)
    .then(message => console.log(`Sent message: ${message.content}`))
    .catch(console.error);
  client.roles_id = Object();
  client.roles_id.bot_role         = client.guilds.cache.first().roles.cache.find(role => role.name === config.roles.bot_role_name).id
  client.roles_id.ppl_to_call_role = client.guilds.cache.first().roles.cache.find(role => role.name === config.roles.ppl_to_call_role_name).id

  commandsJSON = client.commands.map(current_command => {
    return {name:localized.commands_shortcuts[current_command.name].replace(/ /g,"_"),description:current_command.description[config.language].command_description}
  })
  const rest = new REST({ version: '9' }).setToken(token);
  
  rest.put(Routes.applicationCommands(client.application.id), { body: commandsJSON })
    .then(() => console.log('Successfully registered application commands.'))
    .catch(console.error);
  
  client.commands.get("roles_poll" ).execute(client.home_channel, config.language, client.roles_id,client,true)

});

client.commands = getCommands('./commands/')

client.on('messageCreate', message => {
  if(message.author.id === client.user.id) return;
  if(!message.content.includes("<@!"+client.application.id+">") && !message.content.includes("<@&"+client.roles_id.bot_role+">")) return;
  var detected_commands = detectCommands(message)
  if(detected_commands.size > 1){
    var message_text=localized.commands_multiple_matches
    detected_commands.each(current_command => message_text+"\""+current_command.name+"\" ?\n")
    message.channel.send(message_text)
    .then(message => console.log(`Sent message: ${message.content}`))
    .catch(console.error);
    return
  }
  if(detected_commands.size == 1){
    detected_commands.first().execute(client.home_channel, config.language, client.roles_id,client);
    message.delete()
      .then(msg => console.log(`Deleted message from ${msg.author.username}`))
      .catch(console.error);
    return
  }
  message.channel.send(localized.default_answer)
    .then(message => console.log(`Sent message: ${message.content}`))
    .catch(console.error);
});

client.on('interactionCreate', async interaction => {
  try {
    console.log("interactionCreate detected")
    if (!interaction.isCommand()) return;

    const { commandName } = interaction;

  const asked_command = new Array();
  Object.entries(localized.commands_shortcuts).forEach(command => {
    if(command[1].replace(/ /g,"_")===commandName){
      asked_command.push(command)
    }
  })
  if(asked_command.length != 1){
    interaction.reply("Internal error");
  }

    client.commands.get(asked_command[0][0]).execute(client.home_channel, config.language, client.roles_id, client);
    interaction.reply(localized.command_executed.before + asked_command[0][0] + localized.command_executed.after);
  } catch (error) {
    console.error(error)
  }
});

async function close(){
  console.log("close called");
  if(typeof client !== 'undefined'){
    await client.home_channel.send(localized.shutting_down)
  }
  process.exit();
}

process.once('SIGINT', function (code) {
  console.log('SIGINT received...');
  close();
});
process.once('SIGTERM', function (code) {
  console.log('SIGTERM received...');
  close();
});

// Login to Discord with your client's token
client.login(token);